<?php

class FileNotFoundException extends Exception {

    /**
     * FileNotFoundException constructor.
     * @param string|null $message
     * @param int $code
     * @param Exception|null $previous
     */
    public function __construct(string $message = null, $code = 0, Exception $previous = null){
        parent::__construct($message, $code, $previous);
    }
}