<?php

require 'FileNotFoundException.php';

class view
{
    private $path, $controller, $action, $vars;

    /**
     * View constructor
     * @param $path string
     * @param $controllerName string
     * @param $actionName string
     */
    public function __construct(string $path, string $controllerName, string $actionName)
    {
        $this->path       = $path;
        $this->controller = $controllerName;
        $this->action     = $actionName;
    }

    /**
     * Set vars for view
     * @param array $vars
     */
    public function setVars(array $vars)
    {
        foreach ($vars as $key => $val) {
            $this->vars[$key] = $val;
        }
    }

    /**
     * Render view
     * @throws FileNotFoundException
     */
    public function render()
    {
        $fileName = $this->path.DIRECTORY_SEPARATOR.$this->controller.DIRECTORY_SEPARATOR.$this->action.'.phtml';

        if (!file_exists($fileName)) {
            throw new FileNotFoundException($message = "$fileName not found!");
        }

        foreach ($this->vars as $key => $val) {
            $$key = $val;
        }

        include $fileName;
    }
}