<?php
##########################################################
##Game Rules: http://www.drunkmenworkhere.org/185EN.html##
##########################################################

####Load Composer requirements (Logging)####
require 'vendor/autoload.php';
require 'view/View.php';

use Psr\Log\LogLevel;

##########################################################
#######################GAME SETTINGS######################
##########################################################
/**
 *  Define log level
 */
define("LOG_LEVEL", Psr\Log\LogLevel::DEBUG);

/**
 *  Define language
 */
define("LANG", "DE");

/**
 *  Define max rounds
 */
define("ROUNDS", 30);

/**
 *  Define rate for dept and bank
 */
define("RATE", 1.1);

/**
 *  Define probability for drugs
 */
define("DRUGS_AVAILABLE", 20);

/**
 *  Define max drugs per user
 */
define("USER_DRUGS", 20);
/**
 *  Define max guns per user
 */
define("USER_GUNS", 2);

/**
 *  Define max drugs per bitch
 */
define("BITCH_DRUGS", 10);
/**
 *  Define max guns per bitch
 */
define("BITCH_GUNS", 1);
/**
 *  Define price per bitch
 */
define("PRICE_BITCHES", 500);
/**
 *  Define price per bitch
 */
define("PRICE_GUNS", 2000);

/**
 *  Define start message
 */
define("START_MESSAGE", array());
/**
 *  Define start money
 */
define("START_MONEY", 2000);
/**
 *  Define start bank
 */
define("START_BANK", 0);
/**
 *  Define start dept
 */
define("START_DEPT", 5000);
/**
 *  Define start day
 */
define("START_DAY", 0);
/**
 *  Define start health
 */
define("START_HEALTH", 100);
/**
 *  Define start guns
 */
define("START_GUNS", 0);
/**
 *  Define start bitches
 */
define("START_BITCHES", 2);

/**
 *  Define critical bitches limit
 */
define("CRITICAL_BITCHES", 10);
/**
 *  Define critical bank money limit
 */
define("CRITICAL_BANK_MONEY", 1000000);
/**
 *  Define critical dept limit
 */
define("CRITICAL_DEPT", 50000);

/**
 *  Define max bitches
 */
define("MAX_BITCHES", 20);

####DRUGS####
/**
 *  Min ludes price
 */
define("MIN_LUDES", 30);
/**
 *  Max ludes price
 */
define("MAX_LUDES", 90);
/**
 *  Min mda price
 */
define("MIN_MDA", 60);
/**
 *  Max mda price
 */
define("MAX_MDA", 150);

/**
 *  Min speed price
 */
define("MIN_SPEED", 200);
/**
 *  Max speed price
 */
define("MAX_SPEED", 650);

/**
 *  Min cocaine price
 */
define("MIN_COCAINE", 400);
/**
 *  Max cocaine price
 */
define("MAX_COCAINE", 950);

/**
 *  Min acid price
 */
define("MIN_ACID", 1000);
/**
 *  Max acid price
 */
define("MAX_ACID", 1800);

/**
 *  Min heroin price
 */
define("MIN_HEROIN", 1500);
/**
 *  Max heroin price
 */
define("MAX_HEROIN", 2500);

/**
 *  Min hashish price
 */
define("MIN_HASHISH", 1800);
/**
 *  Max hashish price
 */
define("MAX_HASHISH", 3000);

/**
 *  Min peyote price
 */
define("MIN_PEYOTE", 2400);
/**
 *  Max peyote price
 */
define("MAX_PEYOTE", 3700);

/**
 *  Min opium price
 */
define("MIN_OPIUM", 3500);
/**
 *  Max opium price
 */
define("MAX_OPIUM", 5800);

/**
 *  Min weed price
 */
define("MIN_WEED", 4000);
/**
 *  Max weed price
 */
define("MAX_WEED", 7500);
/**
 *  Min loos of health by Police
 */
define("MIN_POLICE_LOOS", 20);
/**
 *  Max loos of health by Police
 */
define("MAX_POLICE_LOOS", 110);
/**
 *  Min loos of health by loan
 */
define("MIN_LOAN_LOOS", 50);
/**
 *  Max loos of health by loan
 */
define("MAX_LOAN_LOOS", 70);
/**
 *  Min loos of bank money by loan
 */
define("MIN_DEA_LOOS", 20000);
/**
 *  Max loos of bank money by loan
 */
define("MAX_DEA_LOOS", 200000);
/**
 *  Chance that police find you
 */
define("CHANCE_OF_POLICE", 10);
/**
 *  Chance of critical situations
 */
define("CRITICAL_CHANCE", 25);

//if(LANG == "DE"){
/**
 *  translation
 */
define("DRUGS", "Drogen");
/**
 *  translation
 */
define("LUDES", "Ludes");
/**
 *  translation
 */
define("MDA", "MDA");
/**
 *  translation
 */
define("SPEED", "Speed");
/**
 *  translation
 */
define("COCAINE", "Kokain");
/**
 *  translation
 */
define("ACID", "LSD");
/**
 *  translation
 */
define("HEROIN", "Heroin");
/**
 *  translation
 */
define("HASHISH", "Haschisch");
/**
 *  translation
 */
define("PEYOTE", "Peyote");
/**
 *  translation
 */
define("OPIUM", "Opium");
/**
 *  translation
 */
define("WEED", "Gras");
/**
 *  translation
 */
define("BRONX", "Bronx");
/**
 *  translation
 */
define("BROOKLYN", "Brooklyn");
/**
 *  translation
 */
define("CENTRAL_PARK", "Central_PARK");
/**
 *  translation
 */
define("CONEY_ISLAND", "Coney Island");
/**
 *  translation
 */
define("GHETTO", "Ghetto");
/**
 *  translation
 */
define("MANHATTAN", "Manhattan");
/**
 *  translation
 */
define("MONEY", "Geld");
/**
 *  translation
 */
define("BANK", "Bank");
/**
 *  translation
 */
define("DEPT", "Schulden");
/**
 *  translation
 */
define("DAY", "Tag");
/**
 *  translation
 */
define("HEALTH", "Gesundheit");
/**
 *  translation
 */
define("GUNS", "Waffen");
/**
 *  translation
 */
define("BITCHES", "Bitches");
/**
 *  translation
 */
define("MESSAGE", "Nachrichten");
/**
 *  translation
 */
define("AVAILABLE_SPACE", "Verfügbarer Platz");
/**
 *  translation
 */
define("DRUGS_AVAILABLE_TEXT", "Erhältliche Drogen");
/**
 *  translation
 */
define("DRUGS_CARRIED_TEXT", "Gekaufte Drogen");
/**
 *  translation
 */
define("GAME_STATE_TEXT", "Spieldaten");
/**
 *  translation
 */
define("SUBMIT", "Senden");
/**
 *  translation
 */
define("BUY", "Kaufen");
/**
 *  translation
 */
define("HEAL", "Heilen");
/**
 *  translation
 */
define("SELL", "Verkaufen");
/**
 *  translation
 */
define("BUYGUN", "Waffe kaufen");
/**
 *  translation
 */
define("DEPOSIT", "Einzahlen");
/**
 *  translation
 */
define("WITHDRAW", "Abheben");
/**
 *  translation
 */
define("CLEAR", "Schulden begleichen");
/**
 *  translation
 */
define("BUYBITCHES", "Bitches kaufen");
/**
 *  translation
 */
define("WIN", "Du hast gewonnen");
/**
 *  translation
 */
define("DEAD", "Du bist gestorben");
/**
 *  translation
 */
define("POLICE", "Die Polizei hat dich verfolgt. Du hast %u leben verloren.");
/**
 *  translation
 */
define("BITCHES_LEFT_YOU", "Eine Bitch ist gegagen");
/**
 *  translation
 */
define("LOAN_SHARK", "Loan Shark hat dich daran erinnert, dass du die Schulden abbezahlen musst. Du hast %u leben verloren.");
/**
 *  translation
 */
define("DEA", "Die DEA hat %u$ beschlagnahmt.");
//}

$logger = new Katzgrau\KLogger\Logger(__DIR__.'/logs', LOG_LEVEL, array (
    'extension' => 'log', // logfile suffix
    'prefix' => 'log_',
    'logFormat' => '[{date}] [{level}]  {message}'
));
##########################################################
#########################PREPARING########################
##########################################################
mt_srand((float) microtime() * 1000000, $mode = MT_RAND_MT19937);

session_start();

logMessage($logger, LogLevel::DEBUG, "GET: ", $_GET);

$player = $_SESSION['player'] ?? setup();

$drugs = array();
$drugs[LUDES] = array(MIN_LUDES, MAX_LUDES);
$drugs[MDA] = array(MIN_MDA, MAX_MDA);
$drugs[SPEED] = array(MIN_SPEED, MAX_SPEED);
$drugs[COCAINE] = array(MIN_COCAINE, MAX_COCAINE);
$drugs[ACID] =  array(MIN_ACID, MAX_ACID);
$drugs[HEROIN] = array(MIN_HEROIN, MAX_HEROIN);
$drugs[HASHISH] = array(MIN_HASHISH, MAX_HASHISH);
$drugs[PEYOTE] = array(MIN_PEYOTE, MAX_PEYOTE);
$drugs[OPIUM] = array(MIN_OPIUM, MAX_OPIUM);
$drugs[WEED] = array(MIN_WEED, MAX_WEED);


$location = $_GET['location'] ?? BRONX;
$action = $_GET['action'] ?? "newday";

logMessage($logger, LogLevel::DEBUG, "location: ", $location);
logMessage($logger, LogLevel::DEBUG, "Action: ", $action);

##########################################################
########################GAME LOGIC########################
##########################################################

####NEW DAY####
if($action == "newday"){
    $player[DAY]++;
    if($player[DAY] > 30)
        $action = "win";
    else {
        $drugs = calculate_price($drugs);
        $player[BANK] = floor($player[BANK] * RATE);
        $player[DEPT] = floor($player[DEPT] * RATE);

        if(rand_boolean(CHANCE_OF_POLICE)){
            $live = mt_rand(MIN_POLICE_LOOS, MAX_POLICE_LOOS * 0.2 * $player[DAY]) / ($player[GUNS] + 1);
            $txt = sprintf(POLICE, $live);
            $player[HEALTH] -= $live;
            writeMessage($player, $txt);
        }
        if($player[BITCHES] >= CRITICAL_BITCHES){
            if(rand_boolean(CRITICAL_CHANCE)){
                $player[BITCHES]--;
                writeMessage($player, POLICE);
            }
        }
        if($player[DEPT] >= CRITICAL_DEPT){
            if(rand_boolean(CRITICAL_CHANCE)){
                $live = mt_rand(MIN_LOAN_LOOS, MAX_LOAN_LOOS);
                $player[HEALTH] -= $live;
                $txt = sprintf(LOAN_SHARK, $live);
                writeMessage($player, $txt);
            }
        }
        if($player[BANK] >= CRITICAL_DEPT){
            if(rand_boolean(CRITICAL_CHANCE)){
                $loos = mt_rand(MIN_DEA_LOOS, MAX_DEA_LOOS);
                $player[BANK] -= $loos;
                $txt = sprintf(DEA, $loos);
                writeMessage($player, $txt);
            }
        }
    }
} else {
    $drugs = unserialize(base64_decode(urldecode($_GET[DRUGS])));
}

logMessage($logger, LogLevel::DEBUG, "Drug prices: ", $drugs);

####LOOSE HEALTH####
if ($player[HEALTH] <= 0)
    $action = "dead";

####BUY DRUG####
if($action == "buy"){
    $drugname = $_GET['buydrug'];
}

if($action == "bought"){
    $action = "newday";
    $count = $_GET['count'];
    if($count != 0){
        $selldrug = $_GET['buydrug'];
        if(array_key_exists($selldrug, $player[DRUGS]))
            $player[DRUGS][$selldrug] += $count;
        else
            $player[DRUGS][$selldrug] = $count;
        $player[MONEY] -= $count * $drugs[$selldrug];
    }
}

####SELL DRUG####
if($action == "sell"){
    $drugname = $_GET['selldrug'];
}

if($action == "sold"){
    $action = "newday";
    $count = $_GET['count'];
    $selldrug = $_GET['selldrug'];
    if (array_key_exists($selldrug, $player[DRUGS])){
        if($count != 0){
            $player[DRUGS][$selldrug] -= $count;
            $player[MONEY] += $count * $drugs[$selldrug];
        }
        if($player[DRUGS][$selldrug] <= 0)
            unset($player[DRUGS][$selldrug]);
    }
}

####DEPOSIT####
if($action == "depositmoney"){
    $action = "newday";
    $count = $_GET['count'];
    if($count != 0){
        $player[MONEY] -= $count;
        $player[BANK] += $count;
    }
}

####WITHDRAW####
if($action == "withdrawmoney"){
    $action = "newday";
    $count = $_GET['count'];
    if($count != 0){
        $player[MONEY] += $count;
        $player[BANK] -= $count;
    }
}

####CLEAR####
if($action == "clearmoney"){
    $action = "newday";
    $count = $_GET['count'];
    if($count != 0){
        $player[MONEY] -= $count;
        $player[DEPT] -= $count;
    }
}

####BUY BITCHES####
if($action == "bitchesbought"){
    $action = "newday";
    $count = $_GET['count'];
    if($count != 0){
        $player[BITCHES] += $count;
        $player[MONEY] -= $count * PRICE_BITCHES;
    }
}

####BUY GUNS####
if($action == "gunbought"){
    $action = "newday";
    $count = $_GET['count'];
    if($count != 0){
        $player[GUNS] += $count;
        $player[MONEY] -= $count * PRICE_GUNS;
    }
}
####HEAL####
if($action == "heal"){
    $action = "newday";
    $player[HEALTH] = 100;
    $player[MONEY] -= floor($player[MONEY] * 0.2);
    $player[BANK] -= floor($player[BANK] * 0.1);
}

$space = USER_DRUGS + BITCH_DRUGS * $player[BITCHES];

$usedSpace = 0;

foreach ($player[DRUGS] as $key => $value){
    $usedSpace += $value;
}

$availableSpace = $space - $usedSpace;

logMessage($logger, LogLevel::DEBUG, "Action: ", $action);

##########################################################
######################VIEW PREPARING######################
##########################################################

$view = new View(__DIR__, "view", $action);
$view->setVars(array(
    "drugname" => $drugname ?? "",
    "location" => $location,
    "messages" => $player[MESSAGE],
    "money" => $player[MONEY],
    "bank" => $player[BANK],
    "dept" => $player[DEPT],
    "day" => $player[DAY],
    "health" => $player[HEALTH],
    "guns" => $player[GUNS],
    "bitches" => $player[BITCHES],
    "drugs" => $drugs,
    "carriedDrugs" => $player[DRUGS],
    "availableSpace" => $availableSpace,
    "space" => $space,
));

try {
    $view->render();
} catch (FileNotFoundException $e) {
    logMessage($logger, LogLevel::ERROR, "ERROR: ", $e);
    header('HTTP/1.1 500 Internal Server Error');
}

##########################################################
######################POSTPROCESSING######################
##########################################################

$_SESSION['player'] = $player;

##########################################################
#########################FUNCTIONS########################
##########################################################

/**
 * Calculate price for drugs
 * @param $drugs array All drugs with min and max price
 * @return array Array
 */
function calculate_price(array $drugs) : array {
    $calcDrugs = array();
    $available = 0;
    foreach ($drugs as $name => $value){
        if(rand_boolean(DRUGS_AVAILABLE / (sizeof($drugs) - $available) + ($available))){
            continue;
        }
        $min = $value[0];
        $max = $value[1];
        $price = mt_rand($min, $max);
        $calcDrugs[$name] = $price;
        $available++;
    }
    return $calcDrugs;
}

/**
 * @param int $probability Probability that it return true in percent
 * @return bool Random boolean
 */
function rand_boolean(int $probability = 50) : bool {
    return mt_rand(0,100) <= $probability;
}

/**
 * Setup Player array
 * @return array Player array
 */
function setup() : array {
    $player = array();
    $player[MESSAGE] = START_MESSAGE;
    $player[MONEY] = START_MONEY;
    $player[BANK] = START_BANK;
    $player[DEPT] = START_DEPT;
    $player[DAY] = START_DAY;
    $player[HEALTH] = START_HEALTH;
    $player[GUNS] = START_GUNS;
    $player[BITCHES] = START_BITCHES;
    $player[DRUGS] = array();
    return $player;
}

/**
 * Write message to player stats;
 * @param array $player Player array
 * @param string $msg message
 */
function writeMessage(array &$player, string $msg){
    array_push($player[MESSAGE], DAY . " " . $player[DAY] . ": " . $msg);
}

/**
 * Logging wrapper
 * @param Psr\Log\AbstractLogger $logger Logger implementation
 * @param string $level logging level
 * @param string $msg Message
 * @param null|object|array $ogj Optional object|array|string|... to attache
 */
function logMessage(Psr\Log\AbstractLogger $logger, string $level, string $msg, $ogj = null){
    if(is_array($ogj))
        $logger->log($level, $msg, $ogj);
    else
        $logger->log($level, $msg . $ogj);
}
